import axios from 'axios';
export const state = () => ({
    authToken: null,
    isAuthenticated: false
})

export const mutations = {
    setToken(state, _token){
        state.authToken = _token;
    }
}

export const actions = {
    getAllShoes(vuexContext, data){
        return new Promise((resolve, reject) => {
            let config = {
                headers: {'X-Auth-Token' : process.env.token}
            };
            axios.post(process.env.apiUrl + '/getShoesList', new FormData() , config)
                .then(response => {
                    resolve(response.data);
                })
                .catch(error => {
                    reject();
                })
        })
    }, 
    addNewShoes(vuexContext, data){
        return new Promise((resolve, reject) => {
            let config = {
                headers: {'X-Auth-Token' : process.env.token}
            };
            let formData = new FormData()
            formData.append('reference', data.reference)
            formData.append('name', data.name)
            formData.append('description', data.description)
            formData.append('quantity', data.quantity)
            formData.append('image', data.image)
            axios.post(process.env.apiUrl + '/addNewShoes', formData, config)
                .then(response => {
                    resolve(response.data);
                })
                .catch(error => {
                    reject();
                })
        })
    }, 
    filterShoes(vuexContext, data){
        return new Promise((resolve, reject) => {
            let config = {
                headers: {'X-Auth-Token' : process.env.token}
            };
            let formData = new FormData()
            formData.append('searchCriteria', data.searchCriteria)
            formData.append('needle', data.needle)
            axios.post(process.env.apiUrl + '/filterShoes', formData, config)
                .then(response => {
                    resolve(response.data);
                })
                .catch(error => {
                    reject();
                })
        })
    },
    getShoesById(vuexContext, data){
        return new Promise((resolve, reject) => {
            let config = {
                headers: {'X-Auth-Token' : process.env.token}
            };
            let formData = new FormData()
            formData.append('shoesId', data.shoesId)
            axios.post(process.env.apiUrl + '/getShoesById', formData, config)
                .then(response => {
                    resolve(response.data);
                })
                .catch(error => {
                    reject();
                })
        })
    },
    updateShoes(vuexContext, data){
        return new Promise((resolve, reject) => {
            let config = {
                headers: {'X-Auth-Token' : process.env.token}
            };
            let formData = new FormData()
            formData.append('shoesId', data.shoesId)
            formData.append('name', data.name)
            formData.append('description', data.description)
            formData.append('quantity', data.quantity)
            formData.append('image', data.image)
            formData.append('status', data.status)
            axios.post(process.env.apiUrl + '/updateShoes', formData, config)
                .then(response => {
                    resolve(response.data);
                })
                .catch(error => {
                    reject();
                })
        })
    }
}

export const getters = {
    getAuthToken: state => {
        return state.authToken;
    },
    isAuthenticated: state => {
        return state.isAuthenticated;
    }
}

export const strict = false